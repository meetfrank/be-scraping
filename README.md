# MeetFrank Backend test task

## **Overview**

The task is to create an app in Node.js which can scrape a job opening from a Spotify career page, store it persistently and serve the stored opening over API.  

**Acceptance criteria:**  
- Extract title, description, location and applyButtonUrl for at least one opening from the Spotify Lever page: https://jobs.lever.co/spotify/  
- Save the extracted data in a database.  
- Expose the stored extracted data using a graphql query.  
    
**Bonus points:**  
- Display the extracted data on page build using React.js.  
  
## **Tech**
**Tech to use for this task:**  
- For back-end please use Node.js and TypeScript.  
- Everything else is up to you.  
- If you decide to do the front-end bonus task, please use React.  


## **Sending completed task**
Please send the completed task to [anton@meetfrank.com](mailto:anton@meetfrank.com) and [fabian@meetfrank.com](mailto:fabian@meetfrank.com).  
It can be in a form of a git repo link, zipped folder or a fileshare link.